var msgHistory = document.querySelector('div.historico')
var sendButton = document.querySelector('.enviar')
var editButtons = document.querySelectorAll('.edit')
var buttonCounter = 0
var caixaMensagem = document.querySelector('textarea')
var editing = false
var currentEditingMsg=0
var msgIds=[]

function addMsg() {
  
    if ((caixaMensagem.value.length > 0)&&!editing) {
        var Mensagem = document.createTextNode(caixaMensagem.value)
        console.log(caixaMensagem.value);
        buttonCounter += 1
        msgIds.push(buttonCounter)
        // criar div da mensagem
        msgHistory.append(document.createElement('div'));
        let currentView = msgHistory.lastChild;
        currentView.classList.add("msgView");
        currentView.classList.add(buttonCounter);

        // criar caixa da mensagem
        currentView.append(document.createElement('div'));
        currentView.lastChild.classList.add('msg')
        currentView.lastChild.classList.add(buttonCounter)
        // adicionar a mensagem na caixa
        currentView.lastChild.appendChild(Mensagem)

        // criar linha de botões
        currentView.append(document.createElement('div'))
        currentView.lastChild.classList.add('buttons')
        // -----criar botões
        currentView = currentView.lastChild
        // --botão de editar
        currentView.append(document.createElement('button'))
        currentView.lastChild.classList.add('edit')
        currentView.lastChild.classList.add(buttonCounter)

        let rotulo = document.createTextNode('editar')
        currentView.lastChild.appendChild(rotulo)

        // Colocar event listener no botão de edit
        let counter = buttonCounter;
        currentView.lastChild.addEventListener('click', function () {
        editMsg(counter);
        });

        //-=----- botão de excluir
        currentView.append(document.createElement('button'));
        currentView.lastChild.classList.add('delete');
        currentView.lastChild.classList.add(buttonCounter);

        rotulo = document.createTextNode('Excluir');
        currentView.lastChild.appendChild(rotulo);

        // colorar event listener no botão de excluir
        currentView.lastChild.addEventListener('click', function () {
        deleteMsg(counter);
        });

  }
  if(editing){
    document.querySelectorAll('.msg')[msgIds.indexOf(currentEditingMsg)].innerHTML=caixaMensagem.value
    caixaMensagem.value=''
    editing=false
  }
}

function editMsg(i){
    caixaMensagem.value=document.querySelectorAll('.msg')[msgIds.indexOf(i)].innerHTML
    editing=true
    currentEditingMsg=i
}

function deleteMsg(i){
    if (confirm("Tem certeza que quer deletar esta mensagem?")) {
        let message=document.querySelectorAll('.msgView')[msgIds.indexOf(i)]
        message.remove()
        msgIds.splice(msgIds.indexOf(i),1)
      }
}



sendButton.addEventListener('click', addMsg);
